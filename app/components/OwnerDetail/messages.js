/*
 * OwnerDetail Messages
 *
 * This contains all the text for the OwnerDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.OwnerDetail.header',
    defaultMessage: 'This is the OwnerDetail component !',
  },
  firstNameLabel: {
    id: 'app.components.OwnerDetail.firstNameLabel',
    defaultMessage: 'First Name',
  },
  firstNamePlaceholder: {
    id: 'app.components.OwnerDetail.firstNamePlaceholder',
    defaultMessage: 'First Name',
  },
  lastNameLabel: {
    id: 'app.components.OwnerDetail.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.OwnerDetail.lastNamePlaceholder',
    defaultMessage: 'Last Name',
  },
  mobilePhoneLabel: {
    id: 'app.components.OwnerDetail.mobilePhoneLabel',
    defaultMessage: 'Mobile Phone',
  },
  mobilePhonePlaceholder: {
    id: 'app.components.OwnerDetail.mobilePlaceholder',
    defaultMessage: 'Mobile Phone #',
  },
  homeAddressLabel: {
    id: 'app.components.OwnerDetail.homeAddressLabel',
    defaultMessage: 'Home Address',
  },
  homeAddressPlaceholder: {
    id: 'app.components.OwnerDetail.homeAddressPlaceholder',
    defaultMessage: 'Home Address',
  },
  cityLabel: {
    id: 'app.components.OwnerDetail.cityLabel',
    defaultMessage: 'City',
  },
  cityPlaceholder: {
    id: 'app.components.OwnerDetail.cityPlaceholder',
    defaultMessage: 'City',
  },
  stateLabel: {
    id: 'app.components.OwnerDetail.stateLabel',
    defaultMessage: 'State',
  },
  statePlaceholder: {
    id: 'app.components.OwnerDetail.statePlaceholder',
    defaultMessage: 'State',
  },
  postalCodeLabel: {
    id: 'app.components.OwnerDetail.postalCodeLabel',
    defaultMessage: 'Postal Code',
  },
  postalCodePlaceholder: {
    id: 'app.components.OwnerDetail.postalCodePlaceholder',
    defaultMessage: 'Postal Code',
  },
  ownershipLabel: {
    id: 'app.components.OwnerDetail.ownershipLabel',
    defaultMessage: '% Ownership',
  },
  ownershipPlaceholder: {
    id: 'app.components.OwnerDetail.ownershipPlaceholder',
    defaultMessage: '% Ownership',
  },
  ssnLabel: {
    id: 'app.components.OwnerDetail.ssnLabel',
    defaultMessage: 'Social Security Number',
  },
  ssnPlaceholder: {
    id: 'app.components.OwnerDetail.ssnPlaceholder',
    defaultMessage: 'Social Security Number',
  },
  emailAddressLabel: {
    id: 'app.components.OwnerDetail.emailAddressLabel',
    defaultMessage: 'Email Address',
  },
  emailAddressPlaceholder: {
    id: 'app.components.OwnerDetail.emailAddressPlaceholder',
    defaultMessage: 'Email Address',
  },
  dateOfBirthLabel: {
    id: 'app.components.OwnerDetail.dateOfBirthLabel',
    defaultMessage: 'Date Of Birth',
  },
});
