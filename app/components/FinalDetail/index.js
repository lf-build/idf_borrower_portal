/**
*
* FinalDetail
*
*/

import React from 'react';
import lodash from 'lodash';
import Spinner from 'components/Spinner';
import { Field, reset } from 'redux-form/immutable';
import { Link } from 'react-router';
import Form from '@ui/Form';
import MoneyField from '@ui/MoneyField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import decodeEntity from 'lib/decodeEntity';
import messages from './messages';

import validateForm from './validate';
const openInNewTab = (url) => {
  window.open(url, '_blank');
};

const renderConsentField = ({ input, label, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className="checkbox">
    <label htmlFor="consents"> <input {...input} placeholder={label} type={type} />
      <Link onClick={() => openInNewTab('/terms-conditions')} className="termsCondition">I agree to the Terms and Conditions </Link>
    </label>
    {touched &&
        ((error && <span><br /><span className="error_message">{error}</span></span>) ||
          (warning && <span>{warning}</span>))}
  </div>
);
const DropDownOptions = (obj) => (
  <option key={obj.key} value={obj.key}>{decodeEntity(obj.value)}</option>
  );

const DropDownList = ({ input, label, options, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className=" form-group">
    <label htmlFor="label3">{label}</label>
    <select className="select_fields" {...input} placeholder={label} type={type}>
      <option key="0"></option>
      {options && Object.entries(options).map(([key, value]) => ({ key, value })).map(DropDownOptions)}
    </select>
    {touched &&
        ((error && <span className="error_message">{error}</span>) ||
          (warning && <span>{warning}</span>))}
  </div>
);

const ButtonPanel = (props) => {
  const { submitting , reset,resetForm} = props; // eslint-disable-line
  return (
    <div className="form-btn pull-right">
      {submitting && <Spinner />}
      <input type="button" disabled={submitting} onClick={() => resetForm()} className="btn submit-btn" value="Cancel" />
      <span>&nbsp;</span>
      <input type="submit" disabled={submitting} className="btn submit-btn" value="Submit" />
    </div>
  );
};
class FinalDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showSpinner: false };
  }
  existingLoanStatus={
    1: 'Yes',
    2: 'No',
  };
  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };
    if (values && values.annualRevenue) {
      if (isNaN(values.annualRevenue)) {
        if (values.annualRevenue.indexOf('$') !== -1 && values.annualRevenue.indexOf(',') !== -1) {
          finalValues.annualRevenue = parseInt(values.annualRevenue.replace(/[$, ]/g, ''), 10);
        }
      } else {
        finalValues.annualRevenue = values.annualRevenue;
      }
    }
    if (values && values.averageBankBalance) {
      if (isNaN(values.averageBankBalance)) {
        if (values.averageBankBalance.indexOf('$') !== -1 && values.averageBankBalance.indexOf(',') !== -1) {
          finalValues.averageBankBalance = parseInt(values.averageBankBalance.replace(/[$, ]/g, ''), 10);
        }
      } else {
        finalValues.averageBankBalance = values.averageBankBalance;
      }
    }
    // Remove null, undefined from payload object
    const payload = lodash.pickBy(this.props.payloadGenerator(finalValues, 'final'), lodash.identity);
    if (payload.BusinessTaxID) {
      payload.BusinessTaxID = payload.BusinessTaxID.replace('-', '');
    }
    payload.ClientIpAddress = this.props.ip;
    this.setState({ showSpinner: true });
    return payload;
  };

  afterSubmit=(err, data) => {
    this.setState({ showSpinner: false });
    this.props.updateStateAfterSubmit(err, data, 'final');
  }
  initialValues = () => {
    const returnValue = {};
    if (this.props.final) {
      return this.props.final;
    }
    return returnValue;
  };
  handleOnLoadError = () => {
   // do not add authCheck here
  };
  resetForm = () => {
    this.props.dispatch(reset('finalDetails'));
  };
  render() {
    const parsedMessages = localityNormalizer(messages);
    return (
      <section id="content4" className="tab-section">
        <Form
          passPropsTo="ButtonPanel"
          afterSubmit={(err, data) => this.afterSubmit(err, data)}
          initialValuesBuilder={(value) => this.initialValues(value)}
          payloadBuilder={(values) => this.modifyJson(values)}
          validate={(values) => validateForm(this, values)}
          name="finalDetails"
          action="api/ca/set-application"
          onLoadError={this.handleOnLoadError}
        >
          <div className="comman_application_wraper">
            <div className="business-details">
              <div className="col-md-4 col-sm-4">
                <MoneyField className="form-control" name="annualRevenue" {...parsedMessages.annualRevenue} />
              </div>
              <div className="col-md-4 col-sm-4">
                <MoneyField className="form-control" name="averageBankBalance" {...parsedMessages.averageBankBalance} />
              </div>
            </div>
            {/* <div className="comman_applicationinner business-details">
              <div className="col-md-4 col-sm-4">
                <TextField className="form-control" name="signature" {...parsedMessages.signature} />
              </div>
            </div>*/}
            <div className="comman_applicationinner business-details">
              <div className="col-md-4 col-sm-4">
                <div className=" form-group">
                  <Field label="Do you have any existing business loans or cash advance?" name="existingLoanStatus" component={DropDownList} options={this.existingLoanStatus} type="select" />
                </div>
              </div></div>
            <div className="comman_applicationinner business-details">
              <div className="col-md-6 col-sm-6">
                <Field name="consents" component={renderConsentField} type="checkbox" />
              </div>
            </div>
            <div className="comman_applicationinner business-details">
              <div className="col-md-4 col-sm-4">{this.state.showSpinner && <Spinner />}</div>
              <ButtonPanel resetForm={this.resetForm} />
            </div>
          </div>
        </Form>
      </section>
    );
  }
}

FinalDetail.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  payloadGenerator: React.PropTypes.any,
  final: React.PropTypes.any,
  updateStateAfterSubmit: React.PropTypes.any,
  ip: React.PropTypes.string,
};

export default FinalDetail;
