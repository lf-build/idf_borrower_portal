import isInRange from 'lib/validation/isInRange';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();

  // Next button validations
 /* if (!values.signature) {
    errors.signature = 'Signature is required';
  } else if (!isInRange(values.signature, 2, 100)) {
    errors.signature = 'Field length must be between 2 and 100';
  }*/
  if (!values.averageBankBalance) {
    errors.averageBankBalance = 'Average Bank Balance is required';
  } else if (!isInRange(isNaN(values.averageBankBalance) ? parseInt(values.averageBankBalance.replace(/[$, ]/g, ''), 10) : values.averageBankBalance, 1, 9999999999999999999)) {
    errors.averageBankBalance = 'Maximum annual revenue is 9999999999999999999';
  }
  if (!values.annualRevenue) {
    errors.annualRevenue = 'Annual Revenue is required';
  } else if (!isInRange(isNaN(values.annualRevenue) ? parseInt(values.annualRevenue.replace(/[$, ]/g, ''), 10) : values.annualRevenue, 1, 9999999999999999999)) {
    errors.annualRevenue = 'Maximum annual revenue is 9999999999999999999';
  }

  if (!values.existingLoanStatus) {
    errors.existingLoanStatus = 'Existing Loan Status is required';
  }
  if (!values.consents) {
    errors.consents = 'Please check consent checkbox';
  }

  return errors;
};

export default validateForm;
