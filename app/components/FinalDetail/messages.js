/*
 * FinalDetail Messages
 *
 * This contains all the text for the FinalDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.FinalDetail.header',
    defaultMessage: 'This is the FinalDetail component !',
  },
  annualRevenueLabel: {
    id: 'app.components.FinalDetail.annualRevenueLabel',
    defaultMessage: 'Annual Revenue',
  },
  annualRevenuePlaceholder: {
    id: 'app.components.FinalDetail.annualRevenuePlaceholder',
    defaultMessage: 'Annual Revenue',
  },
  averageBankBalanceLabel: {
    id: 'app.components.FinalDetail.averageBankBalanceLabel',
    defaultMessage: 'Average Bank Balance',
  },
  averageBankBalancePlaceholder: {
    id: 'app.components.FinalDetail.averageBankBalancePlaceholder',
    defaultMessage: 'Average Bank Balance',
  },
  signatureLabel: {
    id: 'app.components.FinalDetail.signatureLabel',
    defaultMessage: 'Signature',
  },
  signaturePlaceholder: {
    id: 'app.components.FinalDetail.signaturePlaceholder',
    defaultMessage: 'Signature',
  },
});
