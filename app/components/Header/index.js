/**
*
* Header
*
*/

import React from 'react';
import { userSignedOut } from 'sagas/auth/actions';
import { connect } from 'react-redux';
import { browserHistory, Link, withRouter } from 'react-router';
import { injectIntl, intlShape } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import Spinner from 'components/Spinner';
import { getInitialName } from 'components/Helper/functions';
import Logo from 'assets/images/logo.png';
import messages from './messages';
import makeAuthSelector from '../../sagas/auth/selectors';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { collapseNav: '', rightMenu: 'right-menu-hide' };
  }
  toggleCollapse() {
    if (this.state.collapseNav === '') {
      this.setState({ collapseNav: 'open', rightMenu: 'right-menu' });
    } else {
      this.setState({ collapseNav: '', rightMenu: 'right-menu-hide' });
    }
  }
  render() {
    const { emailMode, dispatch, auth: { user } } = this.props;
    const { formatMessage } = this.props.intl;
    if (!emailMode && !user && this.props.location.pathname.split('/')[1] !== 'home') {
      return <Spinner />;
    }
    return (
      <header className="bs-docs-nav navbar fixed" id="top">
        <div className="navbar-header">
          {emailMode ? <span /> : <button aria-controls="bs-navbar" aria-expanded="false" className="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button" onClick={() => { this.toggleCollapse(); }}>
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>}
          <Link className="navbar-brand" to={(this.props.location.pathname.split('/')[1] === 'login') ? '/' : ''}>
            <img role="presentation" src={Logo} />
          </Link>
        </div>
        {emailMode ? <div className="sign-in pull-right"><button type="button" className="sign-btn btn" value="Sign In" onClick={() => { browserHistory.replace('/login'); }}>{formatMessage(messages.signIn)}</button><h4 className="number-bx"> <i className="fa fa-mobile" aria-hidden="true"></i> 855-329-9922</h4></div> :
        <div>
          <nav className={`collapse navbar-collapse pull-right ${this.state.rightMenu}`}>
            <ul className="nav navbar-nav">
              <li className={`profile dropdown ${this.state.collapseNav}`}>
                { user && user.image && <Link className="dropdown-toggle" data-toggle="dropdown" onClick={() => { this.toggleCollapse(); }}><img src={`${user.image}?${Math.random()}`} alt="User Avatar" className="img-res" /><strong> { (!user || !user.userName) ? <span /> : <strong>{user.userName}</strong> }</strong></Link>}
                { user && !user.image && <Link className="dropdown-toggle" data-toggle="dropdown" onClick={() => { this.toggleCollapse(); }}><span className="b-logo-header"><span className="logo-text">{getInitialName(user.userName)}</span></span><div className="detail"><strong> { (!user || !user.userName) ? <span /> : <strong>{user.userName}</strong> }</strong></div></Link>}
                <ul className="dropdown-menu">
                  <li className={this.props.location.pathname.split('/')[1] !== 'home' ? 'responsive-link' : 'hide'}>
                    <Link to="/overview"><span className="menu-icon"><i className="fa fa-user fa-lg"></i></span>
                      <span className="text"> {formatMessage(messages.overview)}</span></Link>
                  </li>
                  <li className={this.props.location.pathname.split('/')[1] !== 'home' ? 'responsive-link' : 'hide'}>
                    <Link to="/todo"><span className="menu-icon"><i className="fa fa-check-square-o fa-lg"></i></span>
                      <span className="text"> {formatMessage(messages.toDo)}</span></Link>
                  </li>
                  <li><Link to="/change-password" className="theme-setting"><i className="fa fa-cog fa-lg"></i> {formatMessage(messages.changePassword)}</Link></li>
                  <li><Link onClick={() => dispatch(userSignedOut())} className="main-link logoutConfirm_open" data-popup-ordinal="0" id="open_93907719"><i className="fa fa-lock fa-lg"></i> {formatMessage(messages.logOut)}</Link></li>
                </ul>
              </li>
            </ul>
          </nav>
          <h4 className="number-bx"> <i className="fa fa-mobile" aria-hidden="true"></i> 855-329-9922 </h4>
        </div>
      }
      </header>);
  }
}

Header.defaultProps = {
  emailMode: false,
};
Header.propTypes = {
  emailMode: React.PropTypes.bool,
  dispatch: React.PropTypes.func,
  intl: intlShape.isRequired,
  auth: React.PropTypes.object,
  location: React.PropTypes.object.isRequired,
};
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}
const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(injectIntl(Header)));
