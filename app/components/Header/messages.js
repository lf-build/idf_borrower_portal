/*
 * Header Messages
 *
 * This contains all the text for the Header component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  changePassword: {
    id: 'app.components.Header.changePassword',
    defaultMessage: 'Change Password',
  },
  logOut: {
    id: 'app.components.Header.logOut',
    defaultMessage: 'Log out',
  },
  overview: {
    id: 'app.components.Header.overview',
    defaultMessage: 'Overview',
  },
  toDo: {
    id: 'app.components.Header.toDo',
    defaultMessage: 'To Do\'s',
  },
  signIn: {
    id: 'app.components.Header.signIn',
    defaultMessage: 'Sign In',
  },
});
