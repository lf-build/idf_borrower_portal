/**
*
* Uploader
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import FileDropZone from 'components/FileDropZone';
import AlertMessage from 'components/AlertMessage';
import { IsUnderFileSizeLimit, IsValidFileFormat } from 'components/Helper/functions';

class Uploader extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, showSpinner: props.showSpinner };
  }
  render() {
    const { files } = this.props.Stipulation;
    const { applicationNumber, uploadFileType, mainHeader, onDropSuccess } = this.props;
    const OnDropDone = (acceptedFiles) => {
      if (acceptedFiles[0].length > 1) {
        this.setState({ isError: true, message: 'Upload 1 file at a time', isVisible: true });
        return;
      }
      if (!IsValidFileFormat(acceptedFiles[0])) {
        this.setState({ isError: true, message: 'Upload only PDF/JPG/PNG file', isVisible: true });
        return;
      }
      if (!IsUnderFileSizeLimit(acceptedFiles[0])) {
        this.setState({ isError: true, message: 'Max file size is 5MB', isVisible: true });
        return;
      }
      const newFileObject = [];
      acceptedFiles[0].forEach((ele) => {
        newFileObject.push({ dataUrl: ele.dataUrl, file: ele.file.toLowerCase() });
      }, this);
      execute(undefined, undefined, ...'api/agreement/upload-engine'.split('/'), {
        applicationNumber,
        uploadFileType,
        fileList: newFileObject,
      })
      .then(() => {
        this.setState({ isVisible: false, showSpinner: true });
        onDropSuccess().then(() => this.setState({ showSpinner: false }));
      }).catch((e) => {
        authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      });
    };
    return (
      <div className="doc-upload-container">
        <h3>{mainHeader}</h3>
        <FileDropZone files={files} applicationNumber={applicationNumber} onDrop={OnDropDone} onDiscard={() => onDropSuccess()} uploadFileType={uploadFileType} />
        {this.state.isVisible ? (() => <span><br /><AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} /></span>)() : <span />}
      </div>
    );
  }
}

Uploader.propTypes = {
  Stipulation: React.PropTypes.any,
  applicationNumber: React.PropTypes.string,
  onDropSuccess: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
  mainHeader: React.PropTypes.string,
  showSpinner: React.PropTypes.bool,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(Uploader));
