/*
 * AlertMessage Messages
 *
 * This contains all the text for the AlertMessage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.AlertMessage.header',
    defaultMessage: 'This is the AlertMessage component !',
  },
});
