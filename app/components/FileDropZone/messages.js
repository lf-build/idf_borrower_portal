/*
 * FileDropZone Messages
 *
 * This contains all the text for the FileDropZone component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  uploadText: {
    id: 'app.components.FileDropZone.uploadText',
    defaultMessage: 'Drop files here to upload',
  },
  uploadButton: {
    id: 'app.components.FileDropZone.uploadButton',
    defaultMessage: 'Upload File',
  },
});
