/**
*
* FileDropZone
*
*
**/

import React from 'react';
import Files from 'components/Files';
import Dropzone from 'react-dropzone';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';

class FileDropZone extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { files, onDrop, applicationNumber, onDiscard, uploadFileType } = this.props;
    const { formatMessage } = this.props.intl;
    const handleFileDropped = (blobs) => {
      const promises = blobs.map((file) => new Promise((resolve) => {
        const reader = new FileReader();
        reader.onload = (e) => {
          resolve({ file: file.name, type: file.type, dataUrl: e.target.result, size: file.size });
        };
        reader.readAsDataURL(file);
      }));
      Promise.all(promises).then((...dataUrls) => onDrop(dataUrls));
    };
    return (
      <div>
        <Files files={files} applicationNumber={applicationNumber} onDiscardFile={() => onDiscard()} uploadFileType={uploadFileType} />
        <Dropzone className="doc-upload-area" onDrop={(blobs) => handleFileDropped(blobs)} >
          <div className="doc-upload-text">
            <button className="btn btn-default pull-right">{formatMessage(messages.uploadButton)}</button>
            <span><i className="fa fa-cloud-upload fa-4x"></i> {formatMessage(messages.uploadText)}</span>
          </div>
        </Dropzone>
      </div>
    );
  }
}

FileDropZone.propTypes = {
  files: React.PropTypes.array.isRequired,
  onDrop: React.PropTypes.func.isRequired,
  applicationNumber: React.PropTypes.string,
  // multiple: React.PropTypes.bool.isRequired,
  onDiscard: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
  intl: intlShape.isRequired,
};

export default injectIntl(FileDropZone);
