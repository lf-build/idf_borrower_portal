/*
 * DocumentUpload Messages
 *
 * This contains all the text for the DocumentUpload component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DocumentUpload.header',
    defaultMessage: 'Upload Documents',
  },
  voidedCheck: {
    id: 'app.components.DocumentUpload.voidedCheck',
    defaultMessage: 'Voided Check',
  },
  photoID: {
    id: 'app.components.DocumentUpload.photoID',
    defaultMessage: 'Photo ID',
  },
  contract: {
    id: 'app.components.DocumentUpload.contract',
    defaultMessage: 'Contract',
  },
  taxReturn: {
    id: 'app.components.DocumentUpload.taxReturn',
    defaultMessage: 'Tax Return',
  },
  confession: {
    id: 'app.components.DocumentUpload.confession',
    defaultMessage: 'Confession',
  },
  lien: {
    id: 'app.components.DocumentUpload.lien',
    defaultMessage: 'Lien',
  },
});
