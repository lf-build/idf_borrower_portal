/**
*
* Spinner
*
*/

import React from 'react';

function Spinner(props) {
  if (props.isSmall) {
    return (
      <div className="small-spinner">
        <div className="double-bounce1"></div>
        <div className="double-bounce2"></div>
      </div>
    );
  }
  return (
    <div className="spinner">
      <div className="double-bounce1"></div>
      <div className="double-bounce2"></div>
    </div>
  );
}

Spinner.propTypes = {
  isSmall: React.PropTypes.bool,
};

export default Spinner;
