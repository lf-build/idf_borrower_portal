import { clearState } from 'sagas/auth/actions';

export default (dispatch) => (e) => (pathName) => {
  if (e && e.status && e.status.code === 401) { // If session expired
    dispatch(clearState(pathName));
  }
};
