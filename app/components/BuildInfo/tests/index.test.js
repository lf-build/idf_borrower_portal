import React from 'react';
import { shallow } from 'enzyme';

import BuildInfo from '../index';
import { name, version } from '../../../../package.json';

describe('<BuildInfo />', () => {
  it('Renders proper version', () => {
    expect(shallow(<BuildInfo />).first().render().text().trim()).toEqual(`${name}@${version}`);
  });
});
