import isInRange from 'lib/validation/isInRange';
import isRegexMatch from 'lib/validation/isRegexMatch';
import { postalCodeRegex, dateRegex, textRegex } from 'lib/validation/regexList';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();

  // Next button validations
  if (!values.legalCompanyName) {
    errors.legalCompanyName = 'Legal Business Name is required';
  } else if (!isInRange(values.legalCompanyName, 2, 100)) {
    errors.legalCompanyName = 'Field length must be between 2 and 100';
  }
  if (!values.businessAddress) {
    errors.businessAddress = 'Business Address is required';
  } else if (!isInRange(values.businessAddress, 2, 100)) {
    errors.businessAddress = 'Field length must be between 2 and 100';
  }
  if (!values.buisnessTaxId) {
    errors.buisnessTaxId = 'Business Tax ID is required';
  } else if (!isInRange(values.buisnessTaxId.replace('-', ''), 7, 7)) {
    errors.buisnessTaxId = 'Field length must be 7';
  }
  if (!values.sicCode) {
    errors.sicCode = 'SIC Code is required';
  } else if (!isInRange(values.sicCode, 4, 4)) {
    errors.sicCode = 'Field length must be 4';
  }
  if (!values.city) {
    errors.city = 'City is required';
  } else if (!isInRange(values.city, 2, 100)) {
    errors.city = 'Field length must be between 2 and 100';
  } else if (!isRegexMatch(values.city.trim(), textRegex)) {
    errors.city = 'Numbers or special characters not allowed';
  }

  if (!values.state) {
    errors.state = 'State is required';
  }
  if (!values.postalCode) {
    errors.postalCode = 'Postal Code is required';
  } else if (!isRegexMatch(values.postalCode, postalCodeRegex)) {
    errors.postalCode = 'Invalid Postal Code';
  }
  if (!values.dateEstablished) {
    errors.dateEstablished = 'Date established is required';
  } else if (!isRegexMatch(values.dateEstablished, dateRegex)) {
    errors.dateEstablished = 'Invalid Date';
  } else {
    const parts = values.dateEstablished.split('/');
    const mydate = new Date(parts[2], parts[0] - 1, parts[1]);
    if (mydate >= new Date()) {
      errors.dateEstablished = 'Date established can not be a future date';
    }
  }

  /* if (!values.addressType) {
    errors.addressType = 'Address type is required';
  }*/

  if (!values.entityType) {
    errors.entityType = 'Entity type is required';
  }

  if (!values.industry) {
    errors.industry = 'Industry is required';
  }

  if (!values.homeOwnerType) {
    errors.homeOwnerType = 'Property type is required';
  }

  return errors;
};

export default validateForm;
