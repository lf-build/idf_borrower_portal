/**
*
* Files
*
*/

import React from 'react';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { downloadFile } from 'components/Helper/functions';
import closeIcon from 'assets/images/close-img.png';
import fileIcon from 'assets/images/file.png';
import Spinner from 'components/Spinner';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';

class Files extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showSpinner: false };
  }
  showSpinner() {
    this.setState({ showSpinner: true });
  }
  hideSpinner() {
    this.setState({ showSpinner: false });
  }
  downloadDocument(fileId, fileName) {
    this.showSpinner();
    execute(undefined, undefined, ...'api/agreement/download-file'.split('/'), {
      applicationNumber: this.props.applicationNumber,
      fileId,
    })
    .then((body) => {
      downloadFile(fileName, body.body.downloadString).then(() => this.hideSpinner());
    }).catch((e) => {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      this.hideSpinner();
    });
  }
  discardDocument(fileId, uploadFileType) {
    this.showSpinner();
    execute(undefined, undefined, ...'api/agreement/discard-document'.split('/'), {
      applicationNumber: this.props.applicationNumber,
      fileId,
      fileType: uploadFileType,
    })
    .then(() => {
      this.props.onDiscardFile().then(() => this.hideSpinner());
    }).catch((e) => {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      this.hideSpinner();
    });
  }
  render() {
    const { files, uploadFileType } = this.props;
    return (
      <div>
        {files.map((file) => (
          <div className="id-icon" key={file.fileId}>
            <span><img src={fileIcon} alt="" /></span>
            <Link onClick={() => this.downloadDocument(file.fileId, file.fileName)} className="tooltip-b">
              <label htmlFor="file-name" className="file-name"> {file.fileName}</label>
            </Link>
            <Link onClick={() => this.discardDocument(file.fileId, uploadFileType)}> <img src={closeIcon} alt="" /></Link>
          </div>
         ))}
        { this.state.showSpinner && <Spinner /> }
      </div>
    );
  }
}

Files.propTypes = {
  files: React.PropTypes.any,
  applicationNumber: React.PropTypes.string,
  onDiscardFile: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(Files));
