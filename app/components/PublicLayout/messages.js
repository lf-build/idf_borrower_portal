/*
 * PublicLayout Messages
 *
 * This contains all the text for the PublicLayout component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.PublicLayout.header',
    defaultMessage: 'This is the PublicLayout component !',
  },
});
