/*
 * BankLink Messages
 *
 * This contains all the text for the BankLink component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.BankLink.header',
    defaultMessage: 'Bank Verification',
  },
  connectButtonHeader: {
    id: 'app.components.BankLink.connectButtonHeader',
    defaultMessage: 'Fast Processing',
  },
  connectButton: {
    id: 'app.components.BankLink.connectButton',
    defaultMessage: 'Link bank account',
  },
  accountNumber: {
    id: 'app.components.BankLink.accountNumber',
    defaultMessage: 'Account Number',
  },
  instituteName: {
    id: 'app.components.BankLink.instituteName',
    defaultMessage: 'Bank Name',
  },
  accountName: {
    id: 'app.components.BankLink.accountName',
    defaultMessage: 'Account Name',
  },
  type: {
    id: 'app.components.BankLink.type',
    defaultMessage: 'Type',
  },
  voidedCheck: {
    id: 'app.components.DocumentUpload.voidedCheck',
    defaultMessage: 'Voided Check',
  },
  uploadThreeBankStatement: {
    id: 'app.components.DocumentUpload.uploadThreeBankStatement',
    defaultMessage: 'Upload 3 recent months bank statements',
  },
});
