/**
*
* BreadCrumb
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';

class BreadCrumb extends React.Component { // eslint-disable-line react/prefer-stateless-function
  capitalizeFirstLetter=(value) => value.charAt(0).toUpperCase() + value.slice(1)
  render() {
    return (
      <div id="breadcrumb">
        <ul className="breadcrumb">
          <li><i className="fa fa-home"></i> <Link to="/overview">Home&nbsp;</Link></li>
          <li><i className="fa fa-home"></i> <span>{this.capitalizeFirstLetter(this.props.location.pathname.split('/')[1])}</span></li>
        </ul>
      </div>
    );
  }
}

BreadCrumb.propTypes = {
  location: React.PropTypes.any,
};

export default connect()(withRouter(BreadCrumb));
