/*
 * BreadCrumb Messages
 *
 * This contains all the text for the BreadCrumb component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.BreadCrumb.header',
    defaultMessage: 'This is the BreadCrumb component !',
  },
});
