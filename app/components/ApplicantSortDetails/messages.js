/*
 * ApplicantSortDetails Messages
 *
 * This contains all the text for the ApplicantSortDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ApplicantSortDetails.header',
    defaultMessage: 'This is the ApplicantSortDetails component !',
  },
  loanApplied: {
    id: 'app.components.ApplicantSortDetails.loanApplied',
    defaultMessage: 'Loan Applied',
  },
  purposeOfFund: {
    id: 'app.components.ApplicantSortDetails.purposeOfFund',
    defaultMessage: 'Purpose of funds',
  },
  app: {
    id: 'app.components.ApplicantSortDetails.app',
    defaultMessage: 'App',
  },
});
