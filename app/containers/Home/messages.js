/*
 * Home Messages
 *
 * This contains all the text for the Home component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Home.header',
    defaultMessage: 'Application Form',
  },
  firstNamePlaceholder: {
    id: 'app.components.Home.firstNamePlaceholder',
    defaultMessage: 'Enter Your first name',
  },
  firstNameLabel: {
    id: 'app.components.Home.firstNameLabel',
    defaultMessage: 'First Name',
  },
  lastNameLabel: {
    id: 'app.components.Home.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.Home.lastNamePlaceholder',
    defaultMessage: 'Enter Your last name!@',
  },
});
