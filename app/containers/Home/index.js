/*
 *
 * Home
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import { createStructuredSelector } from 'reselect';
import Spinner from 'components/Spinner';
import { userSignedIn, loadUserProfile } from 'sagas/auth/actions';
import { browserHistory, withRouter } from 'react-router';
import Layout from 'containers/Layout';
import AlertMessage from 'components/AlertMessage';
import BasicDetail from 'components/BasicDetail';
import BusinessDetail from 'components/BusinessDetail';
import OwnerDetail from 'components/OwnerDetail';
import FinalDetail from 'components/FinalDetail';
import makeAuthSelector from 'sagas/auth/selectors';
import makeSelectHome from './selectors';
import { detectIP } from './actions';

export class Home extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { leadSource: '', activeIndex: 1, isVisible: false, currentTab: 1, applicationNumber: '', applicantId: '', lookup: {}, basic: {}, business: {}, owner: {}, final: {}, rememberMe: false };
  }
  componentWillMount() {
    if (this.props.auth) {
      this.state.rememberMe = this.props.auth.rememberMe;
    }
    // If remember me: then use the token stored in redux-store and go to dashboard
    if (this.props.auth.rememberMe && this.props.auth.user) {
      if (this.props.auth.user.code !== '200.10') {
        browserHistory.replace('/overview');
      }
    } else { // Sign-out otherwise (clear the cache)
      // this.props.dispatch(clearState());
    }
  }
  componentDidMount() {
    this.props.dispatch(detectIP());

    execute(undefined, undefined, ...'api/ca/lookup'.split('/'))
    .then(({ body }) => {
      this.state.lookup = { ...body };
      this.state.sourceType = this.getKey(this.state.lookup.SourceType, this.getLeadSource());
      this.setState(this.state);
    }).catch(() => {
      // Error handling
    });

    // Return applicant flow/ before application submission
    if (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) {
      execute(undefined, undefined, ...'api/ca/get-application'.split('/'), { applicationNumber: this.props.auth.user.applicationNumber })
    .then(({ body }) => {
      const obj = this.getStateObj(body);
      this.state.applicantId = obj.applicantId;
      this.state.applicationNumber = obj.applicationNumber;
      this.state.sourceType = body.source.sourceType;
      this.state.basic = { ...obj.basic };
      this.state.business = { ...obj.business };
      this.state.owner = { ...obj.owner };
      this.state.final = { ...obj.final };
      this.setState(this.state);
      this.state.currentTab = this.getCurrentTab();
      this.state.activeIndex = this.state.currentTab;
      this.setState(this.state);
    }).catch(() => {
      // Error handling
    });
    } else {
      localStorage.clear();
      this.setState({ isVisible: false, currentTab: 1, applicationNumber: '', applicantId: '', lookup: {}, basic: {}, business: {}, owner: {}, final: {} }); // eslint-disable-line
    }
  }

  getKey = (obj, val) => Object.keys(obj).find((key) => obj[key] === val);

  getLeadSource = () => {
    const url = browserHistory.getCurrentLocation();
    if (url && url.query && url.query.lead_source) {
      return url.query.lead_source;
    }
    return 'organic';
  }

  getDateObj=(dateObj) => {
    const dateParts = (dateObj.slice(0, -2)).split('-');
    return `${dateParts[1]}/${dateParts[2]}/${dateParts[0]}`;
  }

  getDateObjj=(dateObj) => {
    const dateParts = (dateObj.split('T'))[0].split('-');
    return `${dateParts[1]}/${dateParts[2]}/${dateParts[0]}`;
  }

  getCurrentTab=() => {
    let currentTab = 1;
    if (this.state.basic.requestedAmount) {
      currentTab = 2;
    }
    if (this.state.business.legalBusinessName) {
      currentTab = 3;
    }
    if (this.state.owner.owners && this.state.owner.owners[0].firstName) {
      currentTab = 4;
    }
    return currentTab;
  }

  getStateObj = (values) => {
    const appObj = {
      applicantId: values.applicantId,
      applicationNumber: values.applicationNumber,
      basic: {
        requestedAmount: values.requestedAmount,
        loanTimeFrame: values.loanTimeFrame,
        purposeOfLoan: values.purposeOfLoan,
        firstName: values.contactFirstName,
        lastName: values.contactLastName,
        primaryPhone: values.phone,
        emailAddress: values.email,
        password: 'Sigma@123',
        confirmPassword: 'Sigma@123',
        // dateNeeded: values.dateNeeded.day.slice(0, -2),
      },
      business: {
        legalCompanyName: values.legalBusinessName,
        businessAddress: values.addressLine1,
        // addressType: '1',
        buisnessTaxId: values.businessTaxID,
        sicCode: values.sicCode,
        city: values.city,
        state: values.state,
        industry: values.industry,
        postalCode: values.zipCode,
        dateEstablished: this.getDateObj(values.businessStartDate.day),
        homeOwnerType: values.propertyType,
        entityType: values.businessType,

      },
      owner: this.getOwnerObject(values),
      final: {
        // signature: values.signature,
        averageBankBalance: values.averageBankBalances,
        annualRevenue: values.annualRevenue,
      },
    };
    return appObj;
  }

  getOwnerObject = (values) => {
    const obj = { owners: [] };
    if (values.owners && values.owners.length > 0) {
      values.owners.map((owner) => {
        const ownerObj = {
          firstName: owner.firstName,
          lastName: owner.lastName,
          mobilePhone: owner.phoneNumbers[0].phone,
          homeAddress: owner.addresses[0].addressLine1,
          emailAddress: owner.emailAddresses[0].email,
          city: owner.addresses[0].city,
          state: owner.addresses[0].state,
          postalCode: owner.addresses[0].zipCode,
          ownership: owner.ownershipPercentage,
          ownershipType: owner.ownershipType,
          ssn: owner.ssn,
          dateOfBirth: this.getDateObjj(owner.dob),
        };
        obj.owners.push(ownerObj);
        return owner;
      });
      return obj;
    }
    return { owners: [{}] };
  }

  getOwnersObj = () => {
    const owners = [];
    if (this.state.owner.owners && this.state.owner.owners.length > 0) {
      this.state.owner.owners.map((owner) => {
        const newOwner = {
          Addresses: [{
            AddressLine1: owner.homeAddress,
            City: owner.city,
            ZipCode: owner.postalCode,
            State: owner.state,
          }],
          DOB: owner.dateOfBirth,
          FirstName: owner.firstName,
          LastName: owner.lastName,
          OwnershipPercentage: owner.ownership,
          OwnershipType: '1',
          EmailAddresses: [{ Email: owner.emailAddress }],
          PhoneNumbers: [{ Phone: owner.mobilePhone }],
          SSN: owner.ssn,
        };
        owners.push(newOwner);
        return true;
      });
    }
    return owners;
  }
  getPayloadObj = () => ({
    Email: (this.state.basic && this.state.basic.emailAddress) ? this.state.basic.emailAddress : null,
    RequestedAmount: (this.state.basic && this.state.basic.requestedAmount) ? this.state.basic.requestedAmount : null,
    PurposeOfLoan: (this.state.basic && this.state.basic.purposeOfLoan) ? this.state.basic.purposeOfLoan : null,
    LoanTimeFrame: (this.state.basic && this.state.basic.loanTimeFrame) ? this.state.basic.loanTimeFrame : null,
    // DateNeeded: this.state.basic.dateNeeded,
    Password: this.state.basic.password,
    LegalBusinessName: this.state.business.legalCompanyName,
    AddressLine1: this.state.business.businessAddress,
    City: this.state.business.city,
    State: this.state.business.state,
    ZipCode: this.state.business.postalCode,
    // AddressType: this.state.business.addressType,
    BusinessTaxID: this.state.business.buisnessTaxId,
    SICCode: this.state.business.sicCode,
    BusinessStartDate: this.state.business.dateEstablished,
    BusinessType: this.state.business.entityType,
    ContactFirstName: (this.state.basic && this.state.basic.firstName) ? this.state.basic.firstName : null,
    ContactLastName: (this.state.basic && this.state.basic.lastName) ? this.state.basic.lastName : null,
    AnnualRevenue: this.state.final.annualRevenue,
    AverageBankBalances: this.state.final.averageBankBalance,
    HaveExistingLoan: (this.state.final.existingLoanStatus === '1') ? 'true' : 'false',
    Source: this.state.sourceType ? this.state.sourceType : '1',  // Organic
    Industry: this.state.business.industry,
    Phone: (this.state.basic && this.state.basic.primaryPhone) ? this.state.basic.primaryPhone : null,
    Owners: this.getOwnersObj(),
    // Signature: this.state.final.signature,
    PropertyType: this.state.business.homeOwnerType,  // Need lookup for this
    LeadOwnerId: '',
    ApplicationNumber: this.state.applicationNumber,
    ApplicantId: this.state.applicantId,
  })

  userAutoLogin= () => {
    const payload = {
      username: this.state.basic.emailAddress,
      password: this.state.basic.password,
      realm: 'borrower-portal',
      client: 'capital-alliance',
      rememberMe: false,
    };
    execute(undefined, undefined, ...'authorization/identity/login'.split('/'), payload)
    .then(({ body }) => {
      this.props.dispatch(userSignedIn(body.token, body.rememberMe));
      this.props.dispatch(loadUserProfile({ userId: body.userId }));
    }).catch(() => {
      // Error handling
    });
  }
  payloadGenerator=(values, callFrom) => {
    if (callFrom === 'basic') {
      this.state.basic = { ...values };
      this.setState(this.state);
    } else if (callFrom === 'business') {
      this.state.business = { ...values };
      this.setState(this.state);
    } else if (callFrom === 'owner') {
      this.state.owner = { ...values };
      this.setState(this.state);
    } else if (callFrom === 'final') {
      this.state.final = { ...values };
      this.setState(this.state);
    }
    return this.getPayloadObj();
  }

  updateStateAfterSubmit=(err, data, callFrom) => {
    if (err) {
      // Error handling code goes here
      this.setState({ isError: true, message: 'Unable to process your request.', isVisible: true, showSpinner: false });
    } else if (data) {
      let redirect = false;
      if (callFrom === 'basic') {
        setTimeout(this.userAutoLogin(), 500);
        this.state.currentTab = 2;
        this.state.activeIndex = 2;
      } else if (callFrom === 'business') {
        this.state.currentTab = 3;
        this.state.activeIndex = 3;
      } else if (callFrom === 'owner') {
        this.state.currentTab = 4;
        this.state.activeIndex = 4;
      } else if (callFrom === 'final') {
        this.state.currentTab = 4;
        this.state.activeIndex = 4;
        redirect = true;
      }
      this.state.applicantId = data.applicantId;
      this.state.applicationNumber = data.applicationNumber;
      this.state.isError = false;
      this.state.isVisible = false;
      this.setState(this.state);
      if (redirect) {
        this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId }));
        browserHistory.replace('/todo');
      }
    }
  }

  displayTab=(index) => {
    if (index <= this.state.currentTab) {
      this.state.activeIndex = index;
      this.setState(this.state);
    }
  }
  render() {
    if (!this.state && !this.state.home) {
      return <Spinner />;
    }
    const emailMode = (this.state.currentTab >= 2) === false;
    return (
      <Layout emailMode={emailMode}>
        <div className="panel panel-default  ">
          <div className="panel-heading">
            <h4 className="m-t-none m-b no-margin">New Application</h4>
          </div>
          <div className="panel-body">
            <div className="row">
              <main>
                <input
                  readOnly className="tab-in" id="tab1" type="radio" name="tabs" onClick={() => this.displayTab(1)}
                  checked={(this.state.currentTab >= 1 && this.state.activeIndex === 1) ? true : ''}
                />
                <label htmlFor="tab1" className="tabbing">Basic</label>
                <input
                  readOnly className="tab-in" id="tab2" type="radio" name="tabs" onClick={() => this.displayTab(2)}
                  checked={(this.state.currentTab >= 2 && this.state.activeIndex === 2) ? true : ''}
                />
                <label htmlFor="tab2" className="tabbing">Business</label>
                <input
                  readOnly className="tab-in" id="tab3" type="radio" name="tabs" onClick={() => this.displayTab(3)}
                  checked={(this.state.currentTab >= 3 && this.state.activeIndex === 3) ? true : ''}
                />
                <label htmlFor="tab3" className="tabbing">Owner</label>
                <input
                  readOnly className="tab-in" id="tab4" type="radio" name="tabs" onClick={() => this.displayTab(4)}
                  checked={(this.state.currentTab >= 4 && this.state.activeIndex === 4) ? true : ''}
                />
                <label htmlFor="tab4" className="tabbing">Finish</label>
                <BasicDetail disableFields={this.state.currentTab > 1} updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} basic={this.state.basic} payloadGenerator={this.payloadGenerator} lookup={this.state.lookup} ip={this.props.Home.ip} />
                <BusinessDetail updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} business={this.state.business} payloadGenerator={this.payloadGenerator} lookup={this.state.lookup} ip={this.props.Home.ip} />
                <OwnerDetail updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} owner={this.state.owner} payloadGenerator={this.payloadGenerator} ip={this.props.Home.ip} />
                <FinalDetail updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} final={this.state.final} payloadGenerator={this.payloadGenerator} ip={this.props.Home.ip} />
              </main>
            </div>
            { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
          </div>
        </div>
      </Layout>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired,
  auth: React.PropTypes.object,
  Home: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  Home: makeSelectHome(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));
