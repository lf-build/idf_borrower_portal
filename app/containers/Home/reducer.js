/*
 *
 * Home reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  IP_DETECTED_SUCCESS,
} from './constants';

const initialState = fromJS({});

function homeReducer(state = initialState, action) {
  switch (action.type) {
    case IP_DETECTED_SUCCESS:
      return state.set('ip', action.ip);
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default homeReducer;
