/*
 *
 * ChangePassword
 *
 */


import React from 'react';
import { connect } from 'react-redux';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import { createStructuredSelector } from 'reselect';
import AppLayout from 'containers/AppLayout';
import AlertMessage from 'components/AlertMessage';
import makeAuthSelector from 'sagas/auth/selectors';
import makeSelectChangePassword from './selectors';

export class ChangePassword extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false };
  }

  sendResetPasswordLink=() => {
    const payload = {
      username: this.props.auth.user.email,
      realm: 'borrower-portal',
    };
    execute(undefined, undefined, ...'authorization/identity/password-reset-init'.split('/'), payload)
    .then(() => {
      this.setState({ isError: false, message: 'Email sent successfully', isVisible: true });
    }).catch(() => {
      this.setState({ isError: true, message: 'Error sending email', isVisible: true });
    });
  }
  render() {
    return (
      <AppLayout appMode>
        <div className="col-sm-12 login-box">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h4 className="m-t-none m-b no-margin">Change Password</h4>
            </div>
            <div className="panel-body">
              <div className="row">
                <div className="business-details">
                  <div className="col-sm-12">
                    <p> This will send reset password link to registered email address.</p>
                  </div>
                </div>
                <div className="form-btn">
                  <button type="button" onClick={this.sendResetPasswordLink} className="btn submit-btn">Reset Password</button>
                </div>
              </div>
            </div>
          </div>
          { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
        </div>
      </AppLayout>
    );
  }
}

ChangePassword.propTypes = {
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  ChangePassword: makeSelectChangePassword(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
