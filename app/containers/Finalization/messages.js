/*
 * Finalization Messages
 *
 * This contains all the text for the Finalization component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Finalization.header',
    defaultMessage: 'This is Finalization container !',
  },
  thanksText: {
    id: 'app.containers.Finalization.thanksText',
    defaultMessage: 'Thanks for submitting the agreement! Here\'s your deal summary:',
  },
  applicantName: {
    id: 'app.containers.Finalization.applicantName',
    defaultMessage: 'Applicant Name',
  },
  businessName: {
    id: 'app.containers.Finalization.businessName',
    defaultMessage: 'Business Name',
  },
  loanAmount: {
    id: 'app.containers.Finalization.loanAmount',
    defaultMessage: 'Loan Amount',
  },
  repaymentAmount: {
    id: 'app.containers.Finalization.repaymentAmount',
    defaultMessage: 'Repayment Amount',
  },
  sellRate: {
    id: 'app.containers.Finalization.sellRate',
    defaultMessage: 'Sell Rate',
  },
  term: {
    id: 'app.containers.Finalization.term',
    defaultMessage: 'Term',
  },
  commission: {
    id: 'app.containers.Finalization.commission',
    defaultMessage: 'Commission',
  },
  whatHappen: {
    id: 'app.containers.Finalization.whatHappen',
    defaultMessage: 'What happens now?',
  },
  whatHappenAnswer: {
    id: 'app.containers.Finalization.whatHappenAnswer',
    defaultMessage: 'Your application is currently being processed for funding. This process usually takes up to 24 hours. Once it is done, the funds will be disbursed to your account.',
  },
});
