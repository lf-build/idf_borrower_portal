/*
 *
 * Finalization
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import { withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import AppLayout from 'containers/AppLayout';
import { createStructuredSelector } from 'reselect';
import makeAuth from 'sagas/auth/selectors';
import Spinner from 'components/Spinner';
import messages from './messages';

export class Finalization extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.auth.user.applicationNumber };
  }
  componentDidMount() {
    execute(undefined, undefined, ...'api/finalization/get-finalization'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(({ body }) => {
      this.setState({ Finalization: body });
      return body;
    }).catch((e) => {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
    });
  }
  render() {
    if (!this.state || !this.state.Finalization) {
      return <Spinner />;
    }
    const { formatMessage } = this.props.intl;
    return (
      <AppLayout appMode>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="m-t-none m-b no-margin">{this.state.Finalization.businessName} </h4>
          </div>
          <div className="panel-body">
            <div className=" col-md-12">
              <p className="thanks-text">{formatMessage(messages.thanksText)}</p>
              <div className="finalization_text">
                <ul className="fine-points">
                  <li className="remove_padd_bottom"><span className="span_left ">{formatMessage(messages.applicantName)}</span> <span className="span_right span_right_name">
                    {this.state.Finalization.applicants.map((applicant, index) => (<label htmlFor="applicant" key={index}>{applicant}</label>))}
                  </span> </li>
                  <li><span className="span_left">{formatMessage(messages.businessName)}</span> <span className="span_right">{this.state.Finalization.businessName}</span> </li>
                  <li><span className="span_left">{formatMessage(messages.loanAmount)}</span> <span className="span_right">${this.state.Finalization.loanAmount}</span> </li>
                  <li><span className="span_left">{formatMessage(messages.repaymentAmount)}</span> <span className="span_right">${this.state.Finalization.repaymentAmount}</span> </li>
                  <li><span className="span_left">{formatMessage(messages.sellRate)}</span><span className="span_right">{this.state.Finalization.sellRate}</span> </li>
                  <li><span className="span_left">{formatMessage(messages.term)}</span><span className="span_right">{this.state.Finalization.term}</span> </li>
                  <li><span className="span_left">{formatMessage(messages.commission)} </span> <span className="span_right">${this.state.Finalization.commission}</span> </li>
                </ul>
              </div>
              <div className="fin-bottom">
                <div className="col-sm-9 no-pad">
                  <i className="fa fa-refresh big_refresh_icon"></i>
                  <h3> {formatMessage(messages.whatHappen)} </h3>
                  <p> {formatMessage(messages.whatHappenAnswer)}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </AppLayout>
    );
  }
}

Finalization.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  auth: React.PropTypes.object,
  intl: intlShape.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(Finalization)));
