/*
 * Profile Messages
 *
 * This contains all the text for the Profile component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Profile.header',
    defaultMessage: 'This is Profile container !',
  },
  firstNameLabel: {
    id: 'app.components.Profile.firstNameLabel',
    defaultMessage: 'First Name',
  },
  firstNamePlaceholder: {
    id: 'app.components.Profile.firstNamePlaceholder',
    defaultMessage: 'First Name',
  },
  lastNameLabel: {
    id: 'app.components.Profile.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.Profile.lastNamePlaceholder',
    defaultMessage: 'Last Name',
  },
  mobilePhoneLabel: {
    id: 'app.components.Profile.mobilePhoneLabel',
    defaultMessage: 'Mobile Phone',
  },
  mobilePhonePlaceholder: {
    id: 'app.components.Profile.mobilePlaceholder',
    defaultMessage: 'Mobile Phone #',
  },
  dateOfBirthLabel: {
    id: 'app.components.Profile.dateOfBirthLabel',
    defaultMessage: 'Date Of Birth',
  },
  dateOfBirthPlaceholder: {
    id: 'app.components.Profile.dateOfBirthPlaceholder',
    defaultMessage: 'Date Of Birth',
  },
  emailAddressLabel: {
    id: 'app.components.OwnerDetails.emailAddressLabel',
    defaultMessage: 'Email Address',
  },
  emailAddressPlaceholder: {
    id: 'app.components.OwnerDetails.emailAddressPlaceholder',
    defaultMessage: 'Email Address',
  },
  cityLabel: {
    id: 'app.components.BusinessDetail.cityLabel',
    defaultMessage: 'City',
  },
  cityPlaceholder: {
    id: 'app.components.BusinessDetail.cityPlaceholder',
    defaultMessage: 'City',
  },
  streetLabel: {
    id: 'app.components.BusinessDetail.streetLabel',
    defaultMessage: 'Street',
  },
  streetPlaceholder: {
    id: 'app.components.BusinessDetail.streetPlaceholder',
    defaultMessage: 'Street',
  },
  postalCodeLabel: {
    id: 'app.components.BusinessDetail.postalCodeLabel',
    defaultMessage: 'Postal Code',
  },
  postalCodePlaceholder: {
    id: 'app.components.BusinessDetail.postalCodePlaceholder',
    defaultMessage: 'Postal Code',
  },
});
