/*
 *
 * Profile
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import lodash from 'lodash';
import Spinner from 'components/Spinner';
import AppLayout from 'containers/AppLayout';
import AlertMessage from 'components/AlertMessage';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import PhoneField from '@ui/PhoneField';
import EmailField from '@ui/EmailField';
import ZipField from '@ui/ZipField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import { createStructuredSelector } from 'reselect';
import makeAuthSelector from 'sagas/auth/selectors';
import authCheck from 'components/Helper/authCheck';
import messages from './messages';
import makeSelectProfile from './selectors';
import validateForm from './validate';

const ButtonPanel = (props) => {  // eslint-disable-line
  const { submitting} = props; // eslint-disable-line
  return (
    <div><div>
      {submitting && <Spinner />}
    </div>
      <div className="form-btn pull-right">
        <input type="submit" disabled={submitting} className="btn submit-btn" value="Submit" />
      </div>
    </div>
  );
};

export class Profile extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, applicationNumber: (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) ? this.props.auth.user.applicationNumber : undefined };
  }
  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };

    if (values && values.mobilePhone) {
      finalValues.mobilePhone = values.mobilePhone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
    }
    finalValues.applicationNumber = this.state.applicationNumber;
    // Remove null, undefined from payload object
    const payload = lodash.pickBy(finalValues, lodash.identity);
    return payload;
  };

  afterSubmit=(e, data) => {
    if (e) {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      this.setState({ isError: true, message: 'Error updating Personal details', isVisible: true });
      return;
    }
    this.setState({ isError: false, message: 'Profile details updated successfully', isVisible: true, initialValues: data });
    // this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId, userName: this.props.auth.user.email, partnerId: this.props.auth.user.partnerId }));
  }
  handleOnLoadError = (e) => {
    authCheck(this.props.dispatch)(e)(this.props.location.pathname);
  };
  render() {
    if (!this.state && !this.state.profile) {
      return <Spinner />;
    }
    const parsedMessages = localityNormalizer(messages);
    return (
      <AppLayout appMode>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="m-t-none m-b no-margin">Profile</h4>
          </div>
          <div className="panel-body">
            <Form
              passPropsTo="ButtonPanel"
              afterSubmit={(err, data) => this.afterSubmit(err, data)}
              initialValuesBuilder={(values) => this.state.initialValues || values}
              payloadBuilder={(values) => this.modifyJson(values)}
              validate={(values) => validateForm(this, values)}
              name="profileDetails"
              action="api/ca/set-profile"
              loadAction={['api/ca/get-profile', { applicationNumber: this.state.applicationNumber }]}
              onLoadError={this.handleOnLoadError}
            >
              <div className="row">
                <div className="business-details">
                  <div className="col-sm-6">
                    <TextField readOnly className="form-control" name="firstName" {...parsedMessages.firstName} />
                  </div>
                  <div className="col-sm-6">
                    <TextField readOnly className="form-control" name="lastName" {...parsedMessages.lastName} />
                  </div>
                  { /*  <div className="col-sm-6">
                    <DateField
                      dateFormat="mm/dd/yyyy"
                      name="dateOfBirth"
                      maxYears={100}
                      {...parsedMessages.dateOfBirth}
                    />
                  </div>*/}
                  <div className="col-sm-6">
                    <EmailField readOnly className="form-control" name="emailAddress" {...parsedMessages.emailAddress} />
                  </div>
                  <div className="col-sm-6">
                    <TextField readOnly className="form-control" name="street" {...parsedMessages.street} />
                  </div>
                  <div className="col-sm-6">
                    <TextField readOnly className="form-control" name="city" {...parsedMessages.city} />
                  </div>
                  <div className="col-sm-6">
                    <ZipField readOnly className="form-control" name="postalCode" {...parsedMessages.postalCode} />
                  </div>
                  <div className="col-sm-6">
                    <PhoneField readOnly className="form-control" name="mobilePhone" {...parsedMessages.mobilePhone} />
                  </div>
                </div>
              </div>
            </Form>
            { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span /> }
          </div>
        </div>
      </AppLayout>
    );
  }
}

Profile.propTypes = {
  auth: React.PropTypes.object,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Profile: makeSelectProfile(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Profile));
