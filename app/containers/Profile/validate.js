import isInRange from 'lib/validation/isInRange';
import isRegexMatch from 'lib/validation/isRegexMatch';
import { emailRegex, postalCodeRegex, mobileRegex } from 'lib/validation/regexList';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();

  if (!values.firstName) {
    errors.firstName = 'First Name is required';
  } else if (!isInRange(values.firstName, 2, 100)) {
    errors.firstName = 'Field length must be between 2 and 100';
  }
  if (!values.lastName) {
    errors.lastName = 'Last Name is required';
  } else if (!isInRange(values.lastName, 2, 100)) {
    errors.lastName = 'Field length must be between 2 and 100';
  }
  if (!values.mobilePhone) {
    errors.mobilePhone = 'Mobile Phone is required';
  } else if (!isRegexMatch(values.mobilePhone.replace(/ /g, ''), mobileRegex)) {
    errors.mobilePhone = 'Invalid Mobile Phone';
  }
  if (!values.emailAddress) {
    errors.emailAddress = 'Email Address is required';
  } else if (!isRegexMatch(values.emailAddress, emailRegex)) {
    errors.emailAddress = 'Invalid Email Address';
  }

  if (!values.city) {
    errors.city = 'City is required';
  } else if (!isInRange(values.city, 2, 100)) {
    errors.city = 'Field length must be between 2 and 100';
  }
  if (!values.street) {
    errors.street = 'Street is required';
  } else if (!isInRange(values.street, 2, 100)) {
    errors.street = 'Field length must be between 2 and 100';
  }
  if (!values.postalCode) {
    errors.postalCode = 'Postal Code is required';
  } else if (!isRegexMatch(values.postalCode, postalCodeRegex)) {
    errors.postalCode = 'Invalid Postal Code';
  }
  if (!values.dateOfBirth) {
    errors.dateOfBirth = 'Date of birth is required';
  }

  return errors;
};

export default validateForm;
