/*
 *
 * TermsAndConditions
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import PublicLayout from 'containers/Layout';
import messages from './messages';

export class TermsAndConditions extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { formatMessage } = this.props.intl;
    return (
      <PublicLayout>
        <div className="col-sm-12 login-box">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h4 className="m-t-none m-b no-margin">{formatMessage(messages.header)}</h4>
            </div>
            <div className="panel-body">
              <div className="row">
                <div className="business-details">
                  <div className="col-sm-12">
                    {formatMessage(messages.mainBody)}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </PublicLayout>
    );
  }
}

TermsAndConditions.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(injectIntl(TermsAndConditions));
