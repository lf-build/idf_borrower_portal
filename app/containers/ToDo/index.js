/*
 *
 * ToDo
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { withRouter, browserHistory } from 'react-router';
import AppLayout from 'containers/AppLayout';
import DocumentUpload from 'components/DocumentUpload';
import BankLink from 'components/BankLink';
import SignDocument from 'components/SignDocument';
import Spinner from 'components/Spinner';
import isDeclined from 'lib/isDeclined';
import makeAuthSelector from 'sagas/auth/selectors';
import makeSelectToDo from './selectors';

export class ToDo extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) ? this.props.auth.user.applicationNumber : undefined };
  }
  componentDidMount() {
    if (this.state.applicationNumber === undefined) {
      redirectWithReturnURL(this.props.location.pathname);
    }
  }
  render() {
    const { auth: { user } } = this.props;
    if (!user) {
      return <Spinner />;
    }
    return (
      <AppLayout appMode>
        { this.props.auth && this.props.auth.user && !isDeclined(this.props.auth.user.code) ? <div className="panel-group todo-panel-group" id="accordion">
          <BankLink applicationNumber={user.applicationNumber} />
          <DocumentUpload applicationNumber={user.applicationNumber} />
          <SignDocument applicationNumber={user.applicationNumber} />
          <div className="todo-box"><button className="btn btn-default" onClick={() => browserHistory.replace('/overview')}>{'I\'ll do it later'}</button></div>
        </div> : <div className="todo-box">
          <p>No action items available.</p>
        </div>
     }
      </AppLayout>
    );
  }
}

ToDo.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  auth: React.PropTypes.object,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ToDo: makeSelectToDo(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ToDo));
