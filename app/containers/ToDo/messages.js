/*
 * ToDo Messages
 *
 * This contains all the text for the ToDo component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ToDo.header',
    defaultMessage: 'This is ToDo container !',
  },
});
