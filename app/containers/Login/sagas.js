import { takeEvery, cancel, take, select } from 'redux-saga/effects';
import makeAuthSelector from 'sagas/auth/selectors';
import { browserHistory } from 'react-router';

function* redirectByStageId() {
  const auth = (yield select(makeAuthSelector()));
  const url = browserHistory.getCurrentLocation();
  if (auth && auth.user && auth.user.code === '200.10') {
    browserHistory.replace(`/home?lead_source=${auth.user.source}`);
  } else if (url && url.query && url.query.return_url) {
    browserHistory.replace(url.query.return_url);
  } else {
    browserHistory.replace('/overview');
  }
}

export function* defaultSaga() {
  const watcher = yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_ENDED_who-am-i', redirectByStageId);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
