/*
 * Login Messages
 *
 * This contains all the text for the Login component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Login.header',
    defaultMessage: 'Login',
  },
  usernamePlaceholder: {
    id: 'app.containers.Login.usernamePlaceholder',
    defaultMessage: 'Email Address',
  },
  usernameLabel: {
    id: 'app.containers.Login.usernameLabel',
    defaultMessage: 'Email Address',
  },
  passwordLabel: {
    id: 'app.containers.Login.passwordLabel',
    defaultMessage: 'Password',
  },
  passwordPlaceholder: {
    id: 'app.containers.Login.passwordPlaceholder',
    defaultMessage: 'Password',
  },
  forgotPassword: {
    id: 'app.containers.Login.forgotPassword',
    defaultMessage: 'Forgot Password?',
  },
  rememberMe: {
    id: 'app.containers.Login.rememberMe',
    defaultMessage: 'Remember Me',
  },
  loginButton: {
    id: 'app.containers.Login.loginButton',
    defaultMessage: 'Log In',
  },
});
