/*
 *
 * Login
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { browserHistory, Link, withRouter } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import PublicLayout from 'components/PublicLayout';
import { isEmail } from 'lib/validation/isEmail';
import { required } from 'lib/validation/required';
import { isInLength } from 'lib/validation/isInLength';
import { injectIntl, intlShape } from 'react-intl';
import AlertMessage from 'components/AlertMessage';
import Spinner from 'components/Spinner';
import { userSignedIn, loadUserProfile } from 'sagas/auth/actions';
import makeAuth from 'sagas/auth/selectors';
import messages from './messages';

export class Login extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, userName: '', password: '', showSpinner: false, rememberMe: false };
  }
  componentWillMount() {
    if (this.props.auth) {
      this.state.rememberMe = this.props.auth.rememberMe;
    }
    // If remember me: then use the token stored in redux-store and go to appropriate page
    if (this.props.auth && this.props.auth.rememberMe && this.props.auth.user) {
      if (this.props.auth.user.code !== '200.10') {
        browserHistory.replace('/overview');
      }
    } else { // Sign-out otherwise (clear the cache)
      // this.props.dispatch(clearState());
    }
  }
  rememberMe = (e) => {
    this.state.rememberMe = e.target.checked;
  }
  render() {
    const { dispatch } = this.props;
    const { formatMessage } = this.props.intl;
    const parsedMessages = localityNormalizer(messages);
    const modifyJason = (values) => {
      const request = {
        ...values,
        realm: 'borrower-portal',
        client: 'capital-alliance',
        rememberMe: (this.state.rememberMe === undefined || this.state.rememberMe === null || this.state.rememberMe === '') ? false : this.state.rememberMe,
      };
      this.setState({
        userName: values.username,
        password: values.password,
        showSpinner: true,
      });
      return request;
    };

    const afterSubmit = (err, values) => {
      if (err) {
        this.setState({ isError: true, message: 'The email address or password you entered is incorrect.', isVisible: true, showSpinner: false });
        return;
      }
      dispatch(userSignedIn(values.token, values.rememberMe));
      dispatch(loadUserProfile({ userId: values.userId }));
    };
    return (
      <PublicLayout>
        <div className="col-sm-5 login-box">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h4 className="m-t-none m-b no-margin">{formatMessage(messages.header)}</h4>
            </div>
            <div className="panel-body">
              <Form initialValuesBuilder={() => ({ username: this.state.userName, password: this.state.password })} afterSubmit={afterSubmit} name="loginForm" action="authorization/identity/login" payloadBuilder={modifyJason}>
                <div className="row">
                  <div className="business-details">
                    <div className="col-sm-6">
                      <TextField inputClassName="form-control" name="username" {...parsedMessages.username} validate={[required(formatMessage(messages.usernameLabel)), isEmail(formatMessage(messages.usernameLabel))]} />
                    </div>
                    <div className="col-sm-6">
                      <TextField type="password" inputClassName="form-control" name="password" {...parsedMessages.password} validate={[required('Password'), isInLength(5, 20)]} />
                    </div>
                    <div className="col-sm-12">
                      <div className="form-group">
                        <div className="col-sm-6 no-pad">
                          <input name="rememberMe" type="checkbox" checked={this.state.rememberMe} onChange={this.rememberMe} />
                          <span>{formatMessage(messages.rememberMe)}</span>
                        </div>
                        <div className="col-sm-6 no-pad">
                          <Link onClick={() => browserHistory.replace('/forgot-password')} className="forgot-pass">{formatMessage(messages.forgotPassword)}</Link>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="form-btn pull-right">
                    <button type="submit" className="btn submit-btn">{formatMessage(messages.loginButton)}</button>
                  </div>
                </div>
              </Form>
            </div>
            { this.state.showSpinner && <Spinner /> }
          </div>
          { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
        </div>
      </PublicLayout>
    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  intl: intlShape.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(Login)));
