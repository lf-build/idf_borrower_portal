/*
 * ResetAccount Messages
 *
 * This contains all the text for the ResetAccount component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ResetAccount.header',
    defaultMessage: 'Reset Account',
  },
  passwordPlaceholder: {
    id: 'app.components.ResetAccount.passwordPlaceholder',
    defaultMessage: 'New Password',
  },
  passwordLabel: {
    id: 'app.containers.ResetAccount.passwordLabel',
    defaultMessage: 'New Password',
  },
  confirmPasswordPlaceholder: {
    id: 'app.components.ResetAccount.confirmPasswordPlaceholder',
    defaultMessage: 'Confirm Password',
  },
  confirmPasswordLabel: {
    id: 'app.containers.ResetAccount.confirmPasswordLabel',
    defaultMessage: 'Confirm Password',
  },
  submitButton: {
    id: 'app.components.ResetAccount.submitButton',
    defaultMessage: 'Submit',
  },
});
