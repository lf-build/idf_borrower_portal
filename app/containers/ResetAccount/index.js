/*
 *
 * ResetAccount
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import { injectIntl, intlShape } from 'react-intl';
import PublicLayout from 'components/PublicLayout';
import authCheck from 'components/Helper/authCheck';
import AlertMessage from 'components/AlertMessage';
import { required } from 'lib/validation/required';
import { isInLength } from 'lib/validation/isInLength';
import { createStructuredSelector } from 'reselect';
import makeSelectResetAccount from './selectors';
import messages from './messages';

export class ResetAccount extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      token: props.location.pathname.split('/')[2],
      username: props.location.pathname.split('/')[3],
      isVisible: false,
      confirmPassword: '',
      password: '',
    };
  }
  modifyJason = (values) => {
    this.setState({
      confirmPassword: values.confirmPassword,
      password: values.password,
    });
    const request = {
      ...values,
      username: this.state.username,
      token: this.state.token,
      realm: 'borrower-portal',
    };
    return request;
  };
  afterSubmitHandle = (e) => {
    if (e) {
      if (e.body.passwordNotMatch) {
        this.setState({ isError: true, message: 'Password does not match the confirm password', isVisible: true });
        return;
      }
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      this.setState({ isError: true, message: 'Something went wrong', isVisible: true });
      return;
    }
    this.setState({ isError: false, message: 'Password updated successfully', isVisible: true });
  };
  render() {
    const { formatMessage } = this.props.intl;
    const parsedMessages = localityNormalizer(messages);
    return (
      <PublicLayout>
        <div className="col-sm-5 login-box">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h4 className="m-t-none m-b no-margin">{formatMessage(messages.header)}</h4>
            </div>
            <div className="panel-body">
              <Form initialValuesBuilder={() => ({ password: this.state.password, confirmPassword: this.state.confirmPassword })} afterSubmit={this.afterSubmitHandle} name="resetAccount" action="authorization/identity/reset-account" payloadBuilder={this.modifyJason}>
                <div className="row">
                  <div className="business-details">
                    <div className="col-sm-12">
                      <TextField inputClassName="form-control" type="password" name="password" {...parsedMessages.password} validate={[required('New Password'), isInLength(5, 20)]} />
                    </div>
                    <div className="col-sm-12">
                      <TextField inputClassName="form-control" type="password" name="confirmPassword" {...parsedMessages.confirmPassword} validate={[required('Confirm Password'), isInLength(5, 20)]} />
                    </div>
                  </div>
                  <div className="form-btn pull-right">
                    <button type="submit" className="btn submit-btn">{formatMessage(messages.submitButton)}</button>
                  </div>
                </div>
              </Form>
            </div>
          </div>
          {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
        </div>
      </PublicLayout>
    );
  }
}

ResetAccount.propTypes = {
  location: React.PropTypes.object.isRequired,
  intl: intlShape.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ResetAccount: makeSelectResetAccount(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(ResetAccount)));
