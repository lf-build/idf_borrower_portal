/*
 *
 * Layout
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Header from 'components/Header';
import appSecure from 'assets/images/app-secure.png';
import arating from 'assets/images/arating.png';

export class Layout extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: React.PropTypes.node,
    emailMode: React.PropTypes.any,
    // dispatch: PropTypes.func.isRequired,
  };
  render() {
    return (
      <div
        style={{
          height: '100%',
          width: '100%',
        }}
      >
        <Header emailMode={this.props.emailMode} />
        <div id="main-wrapper" className="main-wrapper">
          <div className="page-content">
            <div className="row">
              <div className="col-sm-8">
                {React
                  .Children
                  .toArray(this.props.children)}
              </div>
              <div className="col-sm-3">
                <div className="right-images">
                  <a><img alt="" src={appSecure} /></a>
                </div>
                <div className="questions">
                  <h3>Questions?</h3>
                  <p>{'We\'re here to answer any questions or take your application over the phone.'}</p>
                  <div className="que-detail">
                    <h4>855-329-9922</h4>
                    <a>Email Support</a>
                    <a>Chat Live</a>
                  </div>
                </div>
                <div className="about-all">
                  <h3>About Capital Alliance</h3>
                  <p>Capital Alliance was built by the same drive and passion that fuels our clients. We offer simple business loans up to $1 million.</p>
                </div>
                <div className="rating">
                  <a><img alt="" src={arating} /></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Layout.propTypes = {
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(Layout);
