/*
 * Overview Messages
 *
 * This contains all the text for the Overview component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Overview.header',
    defaultMessage: 'Application Details',
  },
  basic: {
    id: 'app.components.Overview.basic',
    defaultMessage: 'Basic',
  },
  business: {
    id: 'app.components.Overview.business',
    defaultMessage: 'Business',
  },
  owner: {
    id: 'app.components.Overview.owner',
    defaultMessage: 'Owner',
  },
  finish: {
    id: 'app.components.Overview.finish',
    defaultMessage: 'Finish',
  },
  firstNameLabel: {
    id: 'app.components.Overview.firstNameLabel',
    defaultMessage: 'First Name',
  },
  lastNameLabel: {
    id: 'app.components.Overview.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  primaryPhoneLabel: {
    id: 'app.components.Overview.primaryPhoneLabel',
    defaultMessage: 'Primary Phone',
  },
  emailAddressLabel: {
    id: 'app.components.Overview.emailAddressLabel',
    defaultMessage: 'Email Address',
  },
  requestedAmountLabel: {
    id: 'app.components.Overview.requestedAmountLabel',
    defaultMessage: 'How much do you need?',
  },
  loanTimeFrame: {
    id: 'app.components.Overview.loanTimeFrame',
    defaultMessage: 'How soon do you need it?',
  },
  purposeOfFund: {
    id: 'app.components.Overview.purposeOfFund',
    defaultMessage: 'Purpose of funds?',
  },
  legalCompanyNameLabel: {
    id: 'app.components.Overview.legalCompanyNameLabel',
    defaultMessage: 'Legal Company Name',
  },
  businessAddressLabel: {
    id: 'app.components.Overview.businessAddressLabel',
    defaultMessage: 'Business Address',
  },
  cityLabel: {
    id: 'app.components.Overview.cityLabel',
    defaultMessage: 'City',
  },
  stateLabel: {
    id: 'app.components.Overview.stateLabel',
    defaultMessage: 'State',
  },
  postalCodeLabel: {
    id: 'app.components.Overview.postalCodeLabel',
    defaultMessage: 'Postal Code',
  },
  mobilePhone: {
    id: 'app.components.Overview.mobilePhone',
    defaultMessage: 'Mobile Phone',
  },
  rentOrOwn: {
    id: 'app.components.Overview.rentOrOwn',
    defaultMessage: 'Rent Or Own this location?',
  },
  industry: {
    id: 'app.components.Overview.industry',
    defaultMessage: 'Industry',
  },
  buisnessTaxIdLabel: {
    id: 'app.components.Overview.buisnessTaxIdLabel',
    defaultMessage: 'Buisness Tax ID',
  },
  sicCodeLabel: {
    id: 'app.components.Overview.sicCodeLabel',
    defaultMessage: 'SIC Code',
  },
  dateOfBirthLabel: {
    id: 'app.components.Overview.dateOfBirthLabel',
    defaultMessage: 'Date Established',
  },
  entityType: {
    id: 'app.components.Overview.entityType',
    defaultMessage: 'Entity Type',
  },
  homeAddress: {
    id: 'app.components.Overview.homeAddress',
    defaultMessage: 'Home Address',
  },
  ofOwnership: {
    id: 'app.components.Overview.ofOwnership',
    defaultMessage: 'of Ownership',
  },
  ssn: {
    id: 'app.components.Overview.ssn',
    defaultMessage: 'Social Security Number',
  },
  ownerEmailAddress: {
    id: 'app.components.Overview.ownerEmailAddress',
    defaultMessage: 'Email Address',
  },
  birthdate: {
    id: 'app.components.Overview.birthdate',
    defaultMessage: 'Birthdate',
  },
  annualRevenueLabel: {
    id: 'app.components.Overview.annualRevenueLabel',
    defaultMessage: 'Annual Revenue',
  },
  averageBankBalanceLabel: {
    id: 'app.components.Overview.averageBankBalanceLabel',
    defaultMessage: 'Average Bank Balance',
  },
  doYouHaveLoan: {
    id: 'app.components.Overview.doYouHaveLoan',
    defaultMessage: 'Do you have any existing business loans or cash advance?',
  },
  signatureLabel: {
    id: 'app.components.Overview.signatureLabel',
    defaultMessage: 'Signature',
  },
  dateNeeedLabel: {
    id: 'app.components.Overview.dateNeeedLabel',
    defaultMessage: 'Date Needed',
  },
  addressTypeLabel: {
    id: 'app.components.Overview.addressTypeLabel',
    defaultMessage: 'Address Type',
  },
  completeYourTodo: {
    id: 'app.components.Overview.completeYourTodo',
    defaultMessage: 'Complete your To Do\'s',
  },
  complete: {
    id: 'app.components.Overview.complete',
    defaultMessage: 'Complete',
  },
});
