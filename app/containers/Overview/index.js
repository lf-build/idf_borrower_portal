/*
 *
 * Overview
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory, withRouter } from 'react-router';
import { injectIntl, intlShape, FormattedNumber } from 'react-intl';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import AppLayout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import { phoneMasking } from 'components/Helper/formatting';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import { createStructuredSelector } from 'reselect';
import makeAuthSelector from 'sagas/auth/selectors';
import isDeclined from 'lib/isDeclined';
import decodeEntity from 'lib/decodeEntity';
import makeSelectOverview from './selectors';
import messages from './messages';

export class Overview extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) ? this.props.auth.user.applicationNumber : undefined, currentTab: 1 };
  }
  componentDidMount() {
    if (this.state.applicationNumber === undefined) {
      redirectWithReturnURL(this.props.location.pathname);
      return; // eslint-disable-line
    }
    execute(undefined, undefined, ...'api/ca/overview'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
      .then(({ body }) => {
        this.setState({ overviewData: body });
      }).catch((e) => {
        authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      });
  }
  changeCurrentTab = (tabIndex) => {
    this.setState({ currentTab: tabIndex });
  }
  render() {
    const { formatMessage } = this.props.intl;
    return (
      <AppLayout appMode>
        <div className="panel panel-default  ">
          <div className="panel-heading">
            <h4 className="m-t-none m-b no-margin">{formatMessage(messages.header)}</h4>
          </div>
          <div className="panel-body">
            <form role="form">
              { this.state.overviewData ?
                <div className="row">
                  <main className="overview">
                    <input className="tab-in" id="tab1" type="radio" name="tabs" checked={this.state.currentTab === 1 ? true : ''} />
                    <label htmlFor="tab1" className="tabbing"><Link onClick={() => this.changeCurrentTab(1)}>{formatMessage(messages.basic)}</Link></label>
                    <input className="tab-in" id="tab2" type="radio" name="tabs" checked={this.state.currentTab === 2 ? true : ''} />
                    <label htmlFor="tab2" className="tabbing"><Link onClick={() => this.changeCurrentTab(2)}>{formatMessage(messages.business)}</Link></label>
                    <input className="tab-in" id="tab3" type="radio" name="tabs" checked={this.state.currentTab === 3 ? true : ''} />
                    <label htmlFor="tab3" className="tabbing"><Link onClick={() => this.changeCurrentTab(3)}>{formatMessage(messages.owner)}</Link></label>
                    <input className="tab-in" id="tab4" type="radio" name="tabs" checked={this.state.currentTab === 4 ? true : ''} />
                    <label htmlFor="tab4" className="tabbing"><Link onClick={() => this.changeCurrentTab(4)}>{formatMessage(messages.finish)}</Link></label>
                    <section id="content1" className="tab-section">
                      <div className="comman_application_wraper">
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.requestedAmountLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText"><FormattedNumber {...{ value: this.state.overviewData.requestedAmount, style: 'currency', currency: 'USD', maximumFractionDigits: 2, minimumFractionDigits: 0 }} /></label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.loanTimeFrame)}</label>
                              <label className="read-only-l" htmlFor="labelText">{decodeEntity(this.state.overviewData.loanTimeFrame)}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.purposeOfFund)}</label>
                              <label className="read-only-l" htmlFor="labelText">{decodeEntity(this.state.overviewData.purposeOfLoan)}</label>
                            </div>
                          </div>
                        </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.firstNameLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.contactFirstName}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.lastNameLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.contactLastName}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.primaryPhoneLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{phoneMasking(this.state.overviewData.phone)}</label>
                            </div>
                          </div>
                        </div>
                        <div className="comman_applicationinner business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.emailAddressLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.email}</label>
                            </div>
                          </div>
                          {/*   <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.dateNeeedLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.dateNeeded ? this.state.overviewData.dateNeeded.day.slice(0, -2) : 'N/A'}</label>
                            </div>
                          </div>*/}
                        </div>
                      </div>
                    </section>
                    <section id="content2" className="tab-section">
                      <div className="comman_application_wraper">
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.legalCompanyNameLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.legalBusinessName}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.businessAddressLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.addressLine1}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.cityLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.city}</label>
                            </div>
                          </div>
                        </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.stateLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.state}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.postalCodeLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.zipCode}</label>
                            </div>
                          </div>

                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.rentOrOwn)}</label>
                              <label className="read-only-l" htmlFor="labelText">{decodeEntity(this.state.overviewData.propertyType)}</label>
                            </div>
                          </div>
                        </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.industry)}</label>
                              <label className="read-only-l" htmlFor="labelText">{decodeEntity(this.state.overviewData.industry)}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.buisnessTaxIdLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.businessTaxID}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.sicCodeLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.sicCode}</label>
                            </div>
                          </div>
                        </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.dateOfBirthLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.businessStartDate.day.slice(0, -2)}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.entityType)}</label>
                              <label className="read-only-l" htmlFor="labelText">{decodeEntity(this.state.overviewData.businessType)}</label>
                            </div>
                          </div>
                          {/* <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.addressTypeLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.addressType ? this.state.overviewData.addressType : 'N/A' }</label>
                            </div>
                          </div>*/}
                        </div>
                      </div>
                    </section>
                    <section id="content3" className="tab-section">
                      {this.state.overviewData.owners.map((owner) => (
                        <div className="comman_application_wraper" key={owner.ownerId}>
                          <div className="form-btn ">
                            <h4 className="heading-text">{formatMessage(messages.owner)} ({`${owner.firstName} ${owner.lastName}`})</h4>
                          </div>
                          <div className="comman_applicationinner business-details ">
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.firstNameLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.firstName}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.lastNameLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.lastName}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.homeAddress)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses[0].addressLine1}</label>
                              </div>
                            </div>
                          </div>
                          <div className="comman_applicationinner business-details">
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.cityLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses[0].city} </label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.stateLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses[0].state}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.postalCodeLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses[0].zipCode} </label>
                              </div>
                            </div>
                          </div>
                          <div className="comman_applicationinner business-details">
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.mobilePhone)}</label>
                                <label className="read-only-l" htmlFor="labelText">{phoneMasking(owner.phoneNumbers[0].phone)}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">% {formatMessage(messages.ofOwnership)}</label>
                                <label className="read-only-l" htmlFor="labelText">% {owner.ownershipPercentage} {formatMessage(messages.ofOwnership)}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.ssn)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.ssn}</label>
                              </div>
                            </div>
                          </div>
                          <div className="comman_applicationinner business-details">
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.ownerEmailAddress)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.emailAddresses && owner.emailAddresses.length > 0 ? owner.emailAddresses[0].email : ''}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.birthdate)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.dob.substring(0, 10)}</label>
                              </div>
                            </div>
                          </div>
                        </div>
                     ))}
                    </section>
                    <section id="content4" className="tab-section">
                      <div className="comman_application_wraper">
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.annualRevenueLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.annualRevenue}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.averageBankBalanceLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.averageBankBalances}</label>
                            </div>
                          </div> </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.doYouHaveLoan)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.haveExistingLoan ? 'Yes' : 'No'}</label>
                            </div>
                          </div>
                        </div>
                        {/* <div className="comman_applicationinner business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.signatureLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData.signature ? this.state.overviewData.signature : 'N/A'}</label>
                            </div>
                          </div>
                        </div>*/}
                      </div>
                    </section>
                  </main>
                  { this.props.auth && this.props.auth.user && !isDeclined(this.props.auth.user.code) && <div className="todo-box">
                    <p>{formatMessage(messages.completeYourTodo)} <button type="button" className="btn todo-btn" value="Complete" onClick={() => browserHistory.replace('/todo')}>{formatMessage(messages.complete)}</button></p>
                  </div> }
                </div>
              : <Spinner />
              }
            </form>
          </div>
        </div>
      </AppLayout>
    );
  }
}

Overview.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  auth: React.PropTypes.object,
  intl: intlShape.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Overview: makeSelectOverview(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(Overview)));
