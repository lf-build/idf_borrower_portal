/*
 *
 * ForgotPassword
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import { createStructuredSelector } from 'reselect';
import { injectIntl, intlShape } from 'react-intl';
import PublicLayout from 'components/PublicLayout';
import AlertMessage from 'components/AlertMessage';
import authCheck from 'components/Helper/authCheck';
import { isEmail } from 'lib/validation/isEmail';
import { required } from 'lib/validation/required';
import makeSelectForgotPassword from './selectors';
import messages from './messages';

export class ForgotPassword extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, userName: '' };
  }
  modifyJason = (values) => {
    this.setState({
      userName: values.username,
    });
    const request = {
      ...values,
      realm: 'borrower-portal',
    };
    return request;
  };
  afterSubmitHandle = (e) => {
    if (e) {
      this.setState({ isError: true, message: 'Error sending mail', isVisible: true });
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      return;
    }
    this.setState({ isError: false, message: 'Email sent successfully', isVisible: true });
  };
  render() {
    const { formatMessage } = this.props.intl;
    const parsedMessages = localityNormalizer(messages);
    return (
      <PublicLayout>
        <div className="col-sm-5 login-box">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h4 className="m-t-none m-b no-margin">{formatMessage(messages.header)}</h4>
            </div>
            <div className="panel-body">
              <Form initialValuesBuilder={() => ({ username: this.state.userName })} afterSubmit={this.afterSubmitHandle} name="forgotPassword" action="authorization/identity/password-reset-init" payloadBuilder={this.modifyJason}>
                <div className="row">
                  <div className="business-details">
                    <div className="col-sm-12">
                      <TextField inputClassName="form-control" name="username" {...parsedMessages.username} validate={[required(formatMessage(messages.usernameLabel)), isEmail(formatMessage(messages.usernameLabel))]} />
                    </div>
                  </div>
                  <div className="form-btn pull-right">
                    <button type="submit" className="btn submit-btn">{formatMessage(messages.submitButton)}</button>
                  </div>
                </div>
              </Form>
            </div>
          </div>
          { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
        </div>
      </PublicLayout>
    );
  }
}

ForgotPassword.propTypes = {
  intl: intlShape.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ForgotPassword: makeSelectForgotPassword(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(ForgotPassword)));
