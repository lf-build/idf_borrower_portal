/*
 * ForgotPassword Messages
 *
 * This contains all the text for the ForgotPassword component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ForgotPassword.header',
    defaultMessage: 'Forgot Password',
  },
  usernamePlaceholder: {
    id: 'app.containers.ForgotPassword.usernamePlaceholder',
    defaultMessage: 'Email Address',
  },
  usernameLabel: {
    id: 'app.containers.ForgotPassword.usernameLabel',
    defaultMessage: 'Email Address',
  },
  submitButton: {
    id: 'app.containers.ForgotPassword.submitButton',
    defaultMessage: 'Submit',
  },
});
