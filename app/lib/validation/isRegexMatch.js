import isEmpty from './isEmpty';

export default(value, regex) => {
  if (isEmpty(value)) {
    return true;
  }
  if (typeof regex === 'string') {
    return new RegExp(regex, 'i').test(value);
  } else if (regex instanceof RegExp) {
    return regex.test(value);
  }
  throw new Error('regex must be an object of type RegExp or string');
};
