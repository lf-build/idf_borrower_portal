export default (a, limit) => {
  const now = new Date();
  const born = new Date(a);
  const age = calculateAge(born, now);
  return age >= limit;
};

function calculateAge(dateOfBirth, dateToCalculate) {
  const calculateYear = dateToCalculate.getFullYear();
  const calculateMonth = dateToCalculate.getMonth();
  const calculateDay = dateToCalculate.getDate();

  const birthYear = dateOfBirth.getFullYear();
  const birthMonth = dateOfBirth.getMonth();
  const birthDay = dateOfBirth.getDate();

  let age = calculateYear - birthYear;
  const ageMonth = calculateMonth - birthMonth;
  const ageDay = calculateDay - birthDay;

  if (ageMonth < 0 || (ageMonth === 0 && ageDay < 0)) {
    age = parseInt(age) - 1; // eslint-disable-line
  }
  return age;
}

