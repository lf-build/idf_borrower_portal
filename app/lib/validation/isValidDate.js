import isRegexMatch from './isRegexMatch';
import { dateRegex } from './regexList';

export const isValidDate = (name) => (value) => isRegexMatch(value, dateRegex) ? undefined : `Invalid ${name}`;
