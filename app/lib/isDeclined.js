// Check for declined
export default (code) => { // eslint-disable-line
  let isDeclined = false;
  try {
    switch (code) {
      case '600.10':
      case '600.20':
      case '600.30':
      case '600.40':
        isDeclined = true;
        break;
      default:
        isDeclined = false;
    }
  } catch (e) {
    // console.log(e);
  }
  return isDeclined;
};
