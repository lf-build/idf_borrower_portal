export default (str) => {
    // this prevents any overhead from creating the object each time
  const element = document.createElement('div');
  let finalResult = '';
  if (str && typeof str === 'string') {
    // strip script/html tags
    let data = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
    data = data.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
    element.innerHTML = data;
    finalResult = element.textContent;
    element.textContent = '';
  }


  return finalResult;
};
