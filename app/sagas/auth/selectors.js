import { createSelector } from 'reselect';

/**
 * Direct selector to the auth state domain
 */
const authDomain = () => (state) => state.get('auth');

/**
 * Other specific selectors
 */


/**
 * Default selector used by SocialPage
 */

const makeAuth = () => createSelector(
  authDomain(),
  (substate) => substate.toJS()
);

export default makeAuth;
export {
  authDomain,
};
